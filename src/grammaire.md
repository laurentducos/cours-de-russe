# Grammaire

# les cas
## datif
## les préposition
liste :
  - *благодаря*: grâce à, à cause de (dans un sens positif)
  - *вопреки*: en dépit, malgré
  - *к (ко)*: vers, jusqu'à
  - *по*: sur, le long, rond, par (communication), les (jours)
exemples :
  - *Я гуляю по деревне* Je me promène dans le village
